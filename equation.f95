program equation
! introduction des variables 
double precision x,f
integer i

open(10,file='EQUATION.dat')

! la boucle de calcule 
do i=0,500
 x=float(i)*0.1d0
 !affichage des resultats 
 write(10,*) i,f(x)
enddo
 
 end

!la fonction   
double precision function f(x)
double precision x
f=(2.4d0)*(dexp(-(3.6d0)*x)/(9.9d0))
return
end
