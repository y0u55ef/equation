# equation

![res](REF.png)

equation **f(x) = 2.4 (exp(-3.6 x)/9.9)**

## License

Licensed under the term of `GPLv3`. See file [gpl-3.0.txt](http://www.gnu.org/licenses/gpl-3.0.txt)
